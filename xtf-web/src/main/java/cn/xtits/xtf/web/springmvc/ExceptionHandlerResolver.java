package cn.xtits.xtf.web.springmvc;

import cn.xtits.xtf.common.exception.XTException;
import cn.xtits.xtf.common.web.AjaxResult;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by Administrator on 2016/1/27.
 */
public class ExceptionHandlerResolver extends SimpleMappingExceptionResolver {
    public static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerResolver.class);


    public ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (ex != null) {
            logger.error("uri={} para={} trace={}",new Object[]{ request.getRequestURI(), getStringRequestParam(request), ExceptionUtils.getStackTrace(ex) });
            AjaxResult ajaxResult = null;
            if (ex instanceof XTException) {
                XTException xtException = (XTException) ex;
                Integer code = xtException.getCode();
                String message = xtException.getMessage();
                ajaxResult = new AjaxResult(code, message, null);
            } else {
                ajaxResult = new AjaxResult(AjaxResult.PARAM_ERROR, ex.getMessage(), null);
            }
            try {
                response.setCharacterEncoding("UTF-8");
                PrintWriter printWriter = response.getWriter();
                printWriter.write(JsonCommonRender.getJson(ajaxResult));
                printWriter.flush();
                printWriter.close();
            } catch (Exception e) {
                logger.error("resolveException eror , e : {}",e);
            }
        }
        return null;
    }

    private String getStringRequestParam(HttpServletRequest request) {
        Enumeration enumeration = request.getParameterNames();
        StringBuffer dataSb = new StringBuffer();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            String data = request.getParameter(key);

            dataSb.append(key).append("=").append(data).append("&");
        }
        if (dataSb.length() > 0)
            dataSb.deleteCharAt(dataSb.length() - 1);

        return dataSb.toString();
    }
}
