package cn.xtits.xtf.web.springmvc;

import org.springframework.util.AntPathMatcher;

import java.util.Map;

/**
 * Created by ShengHaiJiang on 2017/2/28.
 */
public class CaseInsensitivePathMatcher extends AntPathMatcher {
    @Override
    protected boolean doMatch(String pattern, String path, boolean fullMatch, Map<String, String> uriTemplateVariables) {
        return super.doMatch(pattern.toLowerCase(), path.toLowerCase(), fullMatch, uriTemplateVariables);
    }
}
