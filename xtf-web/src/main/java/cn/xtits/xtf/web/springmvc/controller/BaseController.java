package cn.xtits.xtf.web.springmvc.controller;

import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.web.springmvc.xuser.XUser;
import cn.xtits.xtf.web.springmvc.xuser.XUserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ShengHaiJiang on 15/6/25.
 */
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected AjaxResult newAjaxResult() {
        return new AjaxResult();
    }

    protected AjaxResult newAjaxResult(Integer code, String msg, Object data) {
        return new AjaxResult(code, msg, data);
    }

    protected ModelAndView newModelAndView(String viewName) {
        return new ModelAndView(viewName);
    }

    public XUser getXUser() {
        return XUserSession.getCurrent().getXUser();
    }

}
