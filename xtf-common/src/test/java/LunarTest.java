import cn.xtits.xtf.common.date.Lunar;
import cn.xtits.xtf.common.date.SolarTerm;
import org.junit.Test;

/**
 * Created by HaiL on 2017/6/13.
 */
public class LunarTest {

    @Test
    public void getDay() {

        Lunar lunar = new Lunar();
        String date = lunar.getLunarDate(2017, 7, 24);
        System.out.println(date);
    }

    @Test
    public void getSolarTerm() {

        SolarTerm lunar = new SolarTerm();
        lunar.JQtest(2017);
        //lunar.paiYue(2017);
//        q = lunar.jiaoCal(jd + i * 15.2, i * 15, 0);
//        q = q + J2000 + (double) 8 / 24; // 计算第i个节气(i=0是春风),结果转为北京时
//        setFromJD(q, true);
//        s1 = toStr(); // 将儒略日转成世界时
    }

    @Test
    public void JQtest() { // 节气使计算范例,y是年分,这是个测试函数
        for(int y=2000;y<3330;y++) {
            SolarTerm solarTerm = new SolarTerm();
            double jd = 365.2422 * (y - 2000), q;
            String s1, s2;
            int i = 1;
            q = solarTerm.jiaoCal(jd + i * 15.2, i * 15, 0);
            q = q + SolarTerm.J2000 + (double) 8 / 24; // 计算第i个节气(i=0是春风),结果转为北京时
            solarTerm.setFromJD(q, true);
            s1 = solarTerm.toStr(); // 将儒略日转成世界时
            solarTerm.setFromJD(q, false);
            s2 = solarTerm.toStr(); // 将儒略日转成日期格式(输出日期形式的力学时)
            System.out.println(SolarTerm.jqB[i] + " : " + s1 + " " + s2); // 显示
            System.out.println("好了");
        }
    }

}
