package cn.xtits.xtf.common.exception;

/**
 * Created by ShengHaiJiang on 16/4/5.
 */
public class XTDbException extends RuntimeException {
    public static final Integer PARAM_ERROR = 400;

    private Integer code;
    private Throwable t;

    public XTDbException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public XTDbException(String msg) {
        super(msg);
        this.code = PARAM_ERROR;
    }

    public XTDbException(Integer code, String msg, Throwable t) {
        super(msg,t);
        this.code = code;
        this.t = t;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Throwable getT() {
        return t;
    }

    public void setT(Throwable t) {
        this.t = t;
    }
}
