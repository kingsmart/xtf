package cn.xtits.xtf.common.utils;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Random;
import java.util.UUID;

public class Randoms {

    public static String UUID() {
        return StringUtils.replace(UUID.randomUUID().toString(), "-", "");
    }

    public static String nextSeq() {
        return DateFormatUtils.format(System.currentTimeMillis(), "yyMMddHHmmss.SS") + RandomUtils.nextLong(100000000l, 999999999l);
    }

    public static String getRandomString(int length) { //length表示生成字符串的长度
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

}
