package cn.xtits.xtf.common.db.sequence;

/**
 * Created by ShengHaiJiang on 16/4/15.
 */
public interface Sequence {

    public long nextValue(String sequenceName) throws SequenceException;

}
