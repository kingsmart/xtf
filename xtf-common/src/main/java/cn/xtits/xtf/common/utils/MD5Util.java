package cn.xtits.xtf.common.utils;

import java.nio.charset.Charset;
import java.security.MessageDigest;

/**
 * 
 * @author LKL
 * @version V1.0
 * @date 2015年11月30日 下午5:30:18
 */
public class MD5Util {
	private static final String ENCRYPTION_ALGORITHM = "MD5";

	public final static String encode(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			byte[] btInput = s.getBytes(Charset.forName("UTF-8"));
			MessageDigest mdInst = MessageDigest.getInstance(ENCRYPTION_ALGORITHM);
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
