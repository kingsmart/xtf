package cn.xtits.xtf.common.db.sequence;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by ShengHaiJiang on 16/4/15.
 */
public class SequenceRange {
    private long min;
    private long max;
    private volatile boolean isOver;
    private AtomicLong currentValue;

    public SequenceRange(long min, long max) {
        this.min = min;
        this.max = max;
        this.currentValue = new AtomicLong(min);
    }

    public long getAndIncrement() {
        long value = currentValue.incrementAndGet();
        if (value > max) {
            isOver = true;
            value = -1;
        }
        return value;
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public boolean isOver() {
        return isOver;
    }

}
