package cn.xtits.xtf.common.utils;

import cn.xtits.xtf.common.annotaion.FieldMapping;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/3/10.
 */
public class BeanFieldMappingConvert {

    public static void filedValueCopy(Object fromBean,Object toBean) {
        if (fromBean == null || toBean == null)
            throw new RuntimeException("fromBean or toBean can not be null!");


        Class<?> clazzFrom = fromBean.getClass();
        Class<?> clazzTo = toBean.getClass();
        Field[] fields = clazzTo.getFields();
        Map<String,Object> argsMap = new HashMap<String, Object>();
        for (Field field : fields) {
            Object args = null;
            try {
                args = field.getInt(fromBean);
            } catch (Exception e) {
                //TODO
            }
            argsMap.put(field.getName(),args);
        }

        Map<String,MethodArgs> methodMap = new HashMap<String,MethodArgs>();
        Method[] methods = clazzTo.getMethods();
        for (Method m : methods) {
            String methodName = m.getName();
            if (methodName.startsWith("set")) {
                Class<?> paramClazz = m.getParameterTypes()[0];
                String fileName = methodName.substring(3);
                MethodArgs methodParamType = new MethodArgs();
                methodParamType.setMethod(m);
                methodParamType.setArgs(argsMap.get(fileName));
                methodMap.put(fileName.toLowerCase(),methodParamType);
            }
        }

        Field[] fromFields = clazzFrom.getFields();
        for (Field field : fromFields) {
            FieldMapping fieldMapping = field.getAnnotation(FieldMapping.class);
            if (fieldMapping != null) {
                String toFieldName = fieldMapping.toFieldName();
                if (StringUtils.isNotEmpty(toFieldName)) {
                    MethodArgs methodArgs = methodMap.get(toFieldName);
                    if (methodArgs != null) {
                        try {
                            methodArgs.getMethod().invoke(toBean,methodArgs.getArgs());
                        } catch (Exception e) {
                            //TODO
                        }
                    }
                }
            }
        }
    }
}

class MethodArgs{
    private Method method;
    private Object args;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getArgs() {
        return args;
    }

    public void setArgs(Object args) {
        this.args = args;
    }
}

class FromBean {
    private String cjsj;

    public String getCjsj() {
        return cjsj;
    }

    public void setCjsj(String cjsj) {
        this.cjsj = cjsj;
    }
}

class ToBean {
    private String createTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
