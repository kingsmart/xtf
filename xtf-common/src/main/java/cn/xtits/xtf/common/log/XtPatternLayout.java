package cn.xtits.xtf.common.log;

import ch.qos.logback.classic.PatternLayout;

/**
 * Created by 盛海江 on .
 */
public class XtPatternLayout extends PatternLayout {

    static {
        defaultConverterMap.put("ip",HostAddressConvert.class.getName());
        defaultConverterMap.put("host",HostNameConvert.class.getName());
    }
}
