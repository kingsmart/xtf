/**
 * 
 */
package cn.xtits.xtf.common.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTime.Property;
import org.joda.time.Period;
import org.joda.time.PeriodType;

public class JodaDateUtil {

	public static String getFirstDayOfWeek(String specifiedDate, String pattern) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		return dateTime.dayOfWeek().withMinimumValue().toString(pattern);
	}

	public static String getLastDayOfWeek(String specifiedDate, String pattern) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		return dateTime.dayOfWeek().withMaximumValue().toString(pattern);
	}

	public static String getFirstDayOfMonth(String specifiedDate, String pattern) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		return dateTime.dayOfMonth().withMinimumValue().toString(pattern);
	}

	public static String getLastDayOfMonth(String specifiedDate, String pattern) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		return dateTime.dayOfMonth().withMaximumValue().toString(pattern);
	}

	public static int getWeekOfWeekyear(String specifiedDate) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		Property property = dateTime.weekOfWeekyear();

		return property.get();
	}

	public static int getWeekyear(String specifiedDate) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		Property property = dateTime.weekyear();

		return property.get();
	}

	public static String getCurrentDate(String pattern) {
		DateTime dateTime = new DateTime();
		return dateTime.toString(pattern);
	}

	public static String getDateOfSpecified(String specifiedDate, String pattern) {
		DateTime dateTime = DateTime.parse(specifiedDate);
		return dateTime.toString(pattern);
	}

	/**
	 * 计算两日期相关的天数
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int getDiffDays(String startDate, String endDate) {
		DateTime startDateTime = DateTime.parse(startDate);
		DateTime endDateTime = DateTime.parse(endDate);
		Period p = new Period(startDateTime, endDateTime, PeriodType.days());
		return p.getDays() + 1;
	}
}
