package cn.xtits.xtf.common.db;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.commons.lang3.StringUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/3/4.
 */
public class DruidHelper {
    private static HashMap<String,DataSource> connectManager = new HashMap<String,DataSource>();
    private static Object _lockconnectManager = new Object();


    private static HashMap<String, String> getDruidConnectMap(String connectstring) {
        HashMap<String, String> druidMap = new HashMap<String, String>();
        String[] ss = connectstring.split("[|]", -1);
        for (String s : ss) {

            if (StringUtils.isEmpty((s))) {
                continue;
            }

            if (StringUtils.startsWithIgnoreCase(s, "url")) {
                druidMap.put(DruidDataSourceFactory.PROP_URL, StringUtils.removeStartIgnoreCase(s, "url="));
            } else if (StringUtils.startsWithIgnoreCase(s, "username")) {
                druidMap.put(DruidDataSourceFactory.PROP_USERNAME, StringUtils.removeStartIgnoreCase(s, "username="));
            } else if (StringUtils.startsWithIgnoreCase(s, "password")) {
                druidMap.put(DruidDataSourceFactory.PROP_PASSWORD, StringUtils.removeStartIgnoreCase(s, "password="));
            } else if (StringUtils.startsWithIgnoreCase(s, "driverClassName")) {
                druidMap.put(DruidDataSourceFactory.PROP_DRIVERCLASSNAME, StringUtils.removeStartIgnoreCase(s, "driverClassName="));
            } else if (StringUtils.startsWithIgnoreCase(s, "initialSize")) {
                druidMap.put(DruidDataSourceFactory.PROP_INITIALSIZE, StringUtils.removeStartIgnoreCase(s, "initialSize="));
            } else if (StringUtils.startsWithIgnoreCase(s, "maxActive")) {
                druidMap.put(DruidDataSourceFactory.PROP_MAXACTIVE, StringUtils.removeStartIgnoreCase(s, "maxActive="));
            } else if (StringUtils.startsWithIgnoreCase(s, "minIdle")) {
                druidMap.put(DruidDataSourceFactory.PROP_MINIDLE, StringUtils.removeStartIgnoreCase(s, "minIdle="));
            } else if (StringUtils.startsWithIgnoreCase(s, "testWhileIdle")) {
                druidMap.put(DruidDataSourceFactory.PROP_TESTWHILEIDLE, StringUtils.removeStartIgnoreCase(s, "testWhileIdle="));
            } else if (StringUtils.startsWithIgnoreCase(s, "maxWait")) {
                druidMap.put(DruidDataSourceFactory.PROP_MAXWAIT, StringUtils.removeStartIgnoreCase(s, "maxWait="));
            }
        }
        return druidMap;
    }

    public static Connection GetConnection(String connectstring) throws Exception
    {
        if(!connectManager.containsKey(connectstring)) {
            synchronized (_lockconnectManager)
            {
                if(!connectManager.containsKey(connectstring)) {
                    Map<String, String> druidMap = getDruidConnectMap(connectstring);
                    DataSource dataSource = DruidDataSourceFactory.createDataSource(druidMap);
                    //var  GetConnection();
                    connectManager.put(connectstring,dataSource);
                }
            }

        }
        Connection conn = connectManager.get(connectstring).getConnection();
        return  conn;
    }


}

