package cn.xtits.xtf.common.db.sequence.dao;

import cn.xtits.xtf.common.db.sequence.SequenceException;
import cn.xtits.xtf.common.db.sequence.SequenceRange;

/**
 * Created by ShengHaiJiang on 16/4/15.
 */
public interface SequenceDao {

    SequenceRange nextRange(String sequenceName) throws SequenceException;
}
