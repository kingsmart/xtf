package cn.xtits.xtf.common.db;

import org.apache.commons.dbutils.RowProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by ShengHaiJiang on 2016/4/6.
 */
public class XTRowProcessor implements RowProcessor {
    private final BeanConverter convert = new BeanConverter();

    public Object[] toArray(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    public <T> T toBean(ResultSet resultSet, Class<? extends T> aClass) throws SQLException {
        return this.convert.toBean(resultSet, aClass);
    }

    @Override
    public <T> List<T> toBeanList(ResultSet resultSet, Class<? extends T> aClass) throws SQLException {
        return this.convert.toBeanList(resultSet, (Class<T>) aClass);
    }

    @Override
    public Map<String, Object> toMap(ResultSet resultSet) throws SQLException {
        return null;
    }


}
