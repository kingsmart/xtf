package cn.xtits.xtf.common.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by HaiL on 2017/9/1.
 */
public class HostAddressConvert extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        String hostName = "";
        try {
            InetAddress addr = InetAddress.getLocalHost();
            hostName = addr.getHostAddress().toString();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return hostName;
    }
}
