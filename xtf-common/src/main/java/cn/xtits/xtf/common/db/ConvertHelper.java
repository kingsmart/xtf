package cn.xtits.xtf.common.db;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;

/**
 * Created by Administrator on 2016/3/2.
 */
public class ConvertHelper {

    /** 【通用函数】object类型转换为byte类型

     */
    public static Byte objToByte(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        try
        {
            if(aObj instanceof Byte) {
                return (Byte) aObj;
            }
        }
        catch (Exception e)
        {

        }
        return 0;
    }

        /** 【通用函数】object类型转换为short类型

         */
        public static short objToShort(Object aObj)
        {
            if (aObj == null)
            {
                return 0;
            }
            try
            {
                if(aObj instanceof Short) {
                    return (short)aObj;
                }
            }
            catch (Exception e)
            {

            }
            return 0;
        }

        /** 【通用函数】object类型转换为int类型

         */
        public static int objToInt(Object aObj)
        {
            if (aObj == null)
            {
                return 0;
            }
            try
            {
                if(aObj instanceof Integer) {
                    return (int)aObj;
                }
            }
            catch (Exception e)
            {

            }
            return 0;
        }

        /** 【通用函数】object类型转换为Int64类型

         */
        public static long objToInt64(Object aObj)
        {
            if (aObj == null)
            {
                return 0;
            }
            try
            {
                if(aObj instanceof Long) {
                    return (long)aObj;
                }
            }
            catch (Exception e)
            {

            }
            return 0;
        }

        /** 【通用函数】object类型转换为double类型

         */
        public static double objToDouble(Object aObj)
        {
            if (aObj == null)
            {
                return 0;
            }
            try
            {
                if(aObj instanceof Double) {
                    return (double)aObj;
                }
            }
            catch (Exception e)
            {

            }
            return 0;
        }

        /** 【通用函数】object类型转换为decimal类型

         */
        public static BigDecimal objToDecimal(Object aObj)
        {
            if (aObj == null)
            {
                return BigDecimal.ZERO;
            }
            try
            {
                if(aObj instanceof BigDecimal) {
                    return (BigDecimal)aObj;
                }
            }
            catch (Exception e)
            {

            }
            return BigDecimal.ZERO;
        }

        /** 【通用函数】object类型转换为float类型

         */
        public static float objToFloat(Object aObj)
        {
            if (aObj == null)
            {
                return 0;
            }
            float dRet = 0;

            try
            {
                if(aObj instanceof Float) {
                    return (float)aObj;
                }
            }
            catch (Exception e)
            {

            }
            return dRet;
        }


        /** 【通用函数】object类型转换为datetime类型

         */
        public static java.util.Date objToDateTime(Object aObj)
        {
            java.util.Date dRet = new java.util.Date(0);
            if (aObj == null)
            {
                return dRet;
            }

            try
            {
                String str = nullToStr(aObj);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
                return sdf.parse(str);
            }
            catch (Exception e)
            {

            }
            return dRet;
        }

        /** 【通用函数】object类型转换为bool类型，直接强制转换(bool)aObj

         */
        public static boolean objToBool(Object aObj)
        {
            if (aObj == null)
            {
                return false;
            }
            if (aObj instanceof Boolean)
            {
                return ((Boolean)aObj).booleanValue();
            }
            if (aObj.toString().equals("true"))
            {
                return true;
            }
            if (objToInt(aObj) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /** 【通用函数】object类型转换为string类型

         */
        public static String nullToStr(Object aObj)
        {
            return (aObj == null?"":String.valueOf(aObj));
        }
        /** 【通用函数】相当于NullToStr

         */
        public static String objToStr(Object aObj) {
            return (aObj == null ? null : String.valueOf(aObj));
        }

}
