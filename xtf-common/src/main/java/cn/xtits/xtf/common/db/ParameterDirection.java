package cn.xtits.xtf.common.db;

/**
 *
 */
public enum ParameterDirection {
    //     参数是输入参数。
    INPUT(1),

    //     参数是输出参数。
    OUTPUT(2);

    public int value;

    ParameterDirection(int value)
    {
        this.value = value;
    }
}

