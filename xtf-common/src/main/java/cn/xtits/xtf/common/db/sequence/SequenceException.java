package cn.xtits.xtf.common.db.sequence;

/**
 * Created by ShengHaiJiang on 16/4/15.
 */
public class SequenceException extends Exception {

    public SequenceException() {
        super();
    }

    public SequenceException(String message) {
        super(message);
    }

    public SequenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public SequenceException(Throwable cause) {
        super(cause);
    }
}
