package cn.xtits.xtf.common.db;

/**
 * 存储过程参数类
 */
public class ProcedureParameter
{
    /**
     * 参数方向,INPUT或OUTPUT
     */
    private ParameterDirection direction;

    /**
     * direction为OUTPUT时需指定参数对应的数据库类型,
     * 其值为java.sql.Types对应的值
     */
    private int type;

    /**
     * 当direction为INPUT时，为输入值
     * 当direction为OUTPUT时，为输出值
     */
    private Object value;

    private ProcedureParameter(ParameterDirection direction, Object value) {
        this.direction = direction;
        this.value = value;
    }

    private ProcedureParameter(ParameterDirection direction, int type) {
        this.direction = direction;
        this.type = type;
    }

    /**
     * 静态工厂方法，创建输入参数对象
     *
     * @param value
     * @return
     */
    public static ProcedureParameter createInputParameter(Object value) {
       return new ProcedureParameter(ParameterDirection.INPUT, value);
    }

    /**
     * 静态工厂方法，创建输出参数对象
     *
     * @param type
     * @param value
     * @return
     */
    public static ProcedureParameter createOutputParameter(int type) {
        return new ProcedureParameter(ParameterDirection.OUTPUT, type);
    }

    public ParameterDirection getDirection() {
        return direction;
    }

    public void setDirection(ParameterDirection direction) {
        this.direction = direction;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ProcedureParameter{" +
                "direction=" + direction +
                ", type=" + type +
                ", value=" + value +
                '}';
    }
}