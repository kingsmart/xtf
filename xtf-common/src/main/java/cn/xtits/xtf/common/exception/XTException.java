package cn.xtits.xtf.common.exception;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/3/2.
 */
public class XTException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = -416273111872183365L;

    public static final Integer PARAM_ERROR = 400;

    private Integer code;
    private Throwable t;

    public XTException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public XTException(String msg) {
        super(msg);
        this.code = PARAM_ERROR;
    }

    public XTException(Integer code, String msg, Throwable t) {
        super(msg,t);
        this.code = code;
        this.t = t;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Throwable getT() {
        return t;
    }

    public void setT(Throwable t) {
        this.t = t;
    }

    public static void throwEx(String message) {
       throw new XTException(PARAM_ERROR,message);
    }

}
